from datetime import date

date1_entry = input("Enter date(yyyy-mm-dd): ")
date2_entry = input("Enter amother date(yyyy-mm-dd): ")


year1, month1, day1 = map(int, date1_entry.split('-'))
year2, month2, day2 = map(int, date2_entry.split('-'))

date1 = date(year1, month1, day1)
date2 = date(year2, month2, day2)

num_days = abs(date2 - date1)

print(f"{num_days.days} days")
